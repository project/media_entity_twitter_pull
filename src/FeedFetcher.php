<?php

namespace Drupal\media_entity_twitter_pull;

use Psr\Log\LoggerInterface;

/**
 * The tweet feed fetcher service.
 */
class FeedFetcher implements FeedFetcherInterface {

  /**
   * Twitter API factory.
   *
   * @var \Drupal\media_entity_twitter_pull\TwitterAPIFactory
   */
  protected $apiFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a FeedFetcher.
   *
   * @param \Drupal\media_entity_twitter_pull\TwitterAPIFactory $api_factory
   *   Twitter API factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(TwitterAPIFactory $api_factory, LoggerInterface $logger) {
    $this->apiFactory = $api_factory;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserTimelineTweets($username, array $credentials, $count = 200, $since_id = 1) {
    $response = $this->apiFactory
      ->fromCredentials($credentials)
      ->setGetField("?screen_name=$username&count=$count&include_rts=1&since_id=$since_id")
      ->buildOauth('https://api.twitter.com/1.1/statuses/user_timeline.json', 'GET')
      ->performRequest();

    $tweets = [];
    $data = json_decode($response, TRUE);

    if (empty($data['errors'])) {
      foreach ($data as $tweet) {
        $tweets[] = $tweet['id'];
      }
    }
    else {
      $this->logger->error('Error occurred retrieving user timeline for %user: <pre><code>@response</code></pre>', [
        '%user' => $username,
        '@response' => $response,
      ]);
    }

    return $tweets;
  }

}
