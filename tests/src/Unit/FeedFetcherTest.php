<?php

namespace Drupal\Tests\media_entity_twitter_pull\Unit;

use Drupal\media_entity_twitter_pull\FeedFetcher;
use Drupal\media_entity_twitter_pull\TwitterAPIFactory;
use Drupal\Tests\media_entity_twitter_pull\Traits\MediaEntityTwitterPullMockTrait;
use Drupal\Tests\UnitTestCase;
use Psr\Log\LoggerInterface;

/**
 * @coversDefaultClass \Drupal\media_entity_twitter_pull\FeedFetcher
 * @group media_entity_twitter_pull
 */
class FeedFetcherTest extends UnitTestCase {

  use MediaEntityTwitterPullMockTrait;

  /**
   * @covers ::getUserTimelineTweets
   */
  public function testGetUserTimelineTweets() {
    $logger = $this->createMock(LoggerInterface::class);
    $logger->expects($this->once())->method('error');

    $username = $this->randomMachineName();
    $count    = rand(1, 200);
    $since    = rand(201, 500);
    $url      = "?screen_name=$username&count=$count&include_rts=1&since_id=$since";

    $exchange = $this->createApiMock($url, file_get_contents(__DIR__ . '/../../fixtures/user_timeline.json'));
    $factory = $this->createMock(TwitterAPIFactory::class);
    $factory->method('fromCredentials')->willReturn($exchange);
    $result = (new FeedFetcher($factory, $logger))->getUserTimelineTweets($username, [], $count, $since);
    $this->assertArrayEquals([850007368138018817, 848930551989915648], $result, 'Returns a list of numerical tweet IDs.');

    $exchange = $this->createApiMock($url, file_get_contents(__DIR__ . '/../../fixtures/user_timeline_error.json'));
    $factory = $this->createMock(TwitterAPIFactory::class);
    $factory->method('fromCredentials')->willReturn($exchange);
    $result = (new FeedFetcher($factory, $logger))->getUserTimelineTweets($username, [], $count, $since);
    $this->assertEmpty($result, 'Error results in an empty array.');
  }

}
